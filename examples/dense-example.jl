################################################################################
#
#   Objects definition
#
################################################################################

cd(@__DIR__)
using Pkg
Pkg.activate((@__DIR__) * "/..")
using AbstractAlgebra
using Nemo
using ValidatedHensel

prec = 64
setprecision(BigFloat, prec)
set_precision!(Balls, prec)

l = 10

# Q[[X]][Y]/(x^l) for exact computation
QX, x = power_series_ring(AbstractAlgebra.QQ, l, "x"; model=:capped_absolute)
QXY, y = polynomial_ring(QX,"y")

typeof(QX)

# R[[X]][Y]/(x^l) with 'prec' bit of precision for numerical computation
RX, _ = power_series_ring(CFloat, l, "x"; model=:capped_absolute)
RXY, _ = polynomial_ring(RX,"y")

typeof(RX)

# R[[X]][Y]/(x^l) with 'prec' bit of precision for rigorous numerical computation
CBX, _ = power_series_ring(CBall, l, "x"; model=:capped_absolute)
CBXY, _ = polynomial_ring(CBX, "y")

typeof(CBXY)

################################################################################
#
#   Random example 1 
#
################################################################################

G = y^10 + (4//7 + 5*x^7 + 2//5*x^8 + 3//7*x^9)*y^6 + (5//6 + 10//3*x^3 + 9//8*x^4 + 3//7*x^5 + 8//7*x^7 + 1//9*x^8 + 5//3*x^9)*y^5 + (1//2 + 3//10*x^2 + 6*x^3 + 1//5*x^4 + 3//2*x^5 + 5*x^6 + 3*x^7 + 2//3*x^8)*y^4 + (4//3 + 4*x^4 + 5//2*x^5 + 1//5*x^6 + 8//7*x^7 + 3*x^8 + 7//4*x^9)*y^3 + (3//2)*y^2 + (1//5 + 3//10*x^3 + 3//5*x^4 + 3//2*x^5 + 2*x^6 + 5//3*x^7 + x^8 + 5//9*x^9)*y + 1//2 + 4*x^7 + 3//2*x^8 + 2*x^9

H = y^10 + (7//3 + 7//8*x^7 + 7*x^8 + 8*x^9)*y^8 + (8 + 9//8*x^7 + 2//3*x^8 + 3//4*x^9)*y^7 + (3//4 + 3*x^3 + 5//3*x^4 + 10*x^5 + 3//2*x^7 + 4*x^8 + 3//5*x^9)*y^6 + (4//9 + 2//7*x + 1//10*x^2 + x^3 + 9//8*x^4 + 4//9*x^5 + 7//10*x^6 + 3//5*x^7 + 9//7*x^8 + 2*x^9)*y^5 + (8//5 + 1//2*x^2 + 10//3*x^3 + x^4 + 1//3*x^5 + 2//7*x^6 + 5//7*x^7 + 5//2*x^8 + 5*x^9)*y^4 + (2//5 + 9//8*x^3 + 5//2*x^4 + x^5 + 7//6*x^6 + 5//8*x^7 + 3*x^8 + 7//4*x^9)*y^3 + (10 + 1//2*x^3 + 9//4*x^4 + 3//10*x^5 + 3*x^6 + 7//2*x^7 + 6//7*x^8 + 8*x^9)*y^2 + (3//2 + 6//7*x^2 + 2*x^3 + 6*x^5 + 3//2*x^6 + 2*x^7 + 5//2*x^8 + 8//9*x^9)*y + 5//2 + 4//5*x^6 + 4//7*x^7 + 5//4*x^8 + 9//10*x^9

m = G.length-1
n = H.length-1

F = biv_mullow(G, H, l)

biv_truncate(F - G * H, l)

g = biv_truncate(G, 1)
h = biv_truncate(H, 1)
gcd_g_h, s, t = gcdx(g,h)

# Sanity checks
s*g + t*h
biv_truncate(F - g*h, 1)

######################### Exact symbolic computation ########################
@time (G_e, H_e, S_e, T_e) = hensel_lifting(F, g, h, s, t, l)
S = S_e
T = T_e

# Sanity checks
G_e - G
H_e-H
biv_mullow(S_e,G_e,l) + biv_mullow(T_e,H_e,l)


######################### Numerical approximate computation ########################
F_tilde = to_other_poly(F, CFloat, RX, RXY)
g_tilde = to_other_poly(g, CFloat, RX, RXY)
h_tilde = to_other_poly(h, CFloat, RX, RXY)
s_tilde = to_other_poly(s, CFloat, RX, RXY)
t_tilde = to_other_poly(t, CFloat, RX, RXY)

# Choice of converging radiuses
rho = 1.0
tau = 1.0

# numerical computation without overwriting nor refinement
(G_tilde1, H_tilde1, S_tilde1, T_tilde1) = intermediate_hensel_lifting(F_tilde, g_tilde, h_tilde, s_tilde, t_tilde, l)
errG1 = Float64(biv_norm(to_other_poly(G_tilde1, CBall, CBX, CBXY) - to_other_poly(G, CBall, CBX, CBXY), rho, tau))
errH1 = Float64(biv_norm(to_other_poly(H_tilde1, CBall, CBX, CBXY) - to_other_poly(H, CBall, CBX, CBXY), rho, tau))

# numerical computation with overwriting but no refinement
(G_tilde2, H_tilde2, S_tilde2, T_tilde2) = hensel_lifting(F_tilde, g_tilde, h_tilde, s_tilde, t_tilde, l)
errG2 = Float64(biv_norm(to_other_poly(G_tilde2, CBall, CBX, CBXY) - to_other_poly(G, CBall, CBX, CBXY), rho, tau))
errH2 = Float64(biv_norm(to_other_poly(H_tilde2, CBall, CBX, CBXY) - to_other_poly(H, CBall, CBX, CBXY), rho, tau))

# numerical computation with overwriting and refinement
(G_tilde3, H_tilde3, S_tilde3, T_tilde3) = hensel_lifting(F_tilde, g_tilde, h_tilde, s_tilde, t_tilde, l, refine=true)
errG3 = Float64(biv_norm(to_other_poly(G_tilde3, CBall, CBX, CBXY) - to_other_poly(G, CBall, CBX, CBXY), rho, tau))
errH3 = Float64(biv_norm(to_other_poly(H_tilde3, CBall, CBX, CBXY) - to_other_poly(H, CBall, CBX, CBXY), rho, tau))


######################### Numerically validated computation ########################
F_int = to_other_poly(F, CBall, CBX, CBXY)
g_int = to_other_poly(g, CBall, CBX, CBXY)
h_int = to_other_poly(h, CBall, CBX, CBXY)
s_int = to_other_poly(s, CBall, CBX, CBXY)
t_int = to_other_poly(t, CBall, CBX, CBXY)
G_int = to_other_poly(G, CBall, CBX, CBXY)
H_int = to_other_poly(H, CBall, CBX, CBXY)
S_int = to_other_poly(S, CBall, CBX, CBXY)
T_int = to_other_poly(T, CBall, CBX, CBXY)

# first method, rewriting+refinement
val_hensel_lifting_interval(F_int, g_int, h_int, s_int, t_int, l, rho, tau, refine=true)

# alt method, rewriting+refinement
(G_int1, H_int1, r1) = val_hensel_lifting_alt_interval(F_int, g_int, h_int, s_int, t_int, l, rho, tau, refine=true)
r1

# alt method, rewriting but no refinement
(G_int2, H_int2, r2) = val_hensel_lifting_alt_interval(F_int, g_int, h_int, s_int, t_int, l, rho, tau, refine=false)
r2


# direct interval computation
(G_int0, H_int0, S_int0, T_int0) = intermediate_hensel_lifting(F_int, g_int, h_int, s_int, t_int, l)
rG0 = Float64(biv_norm(G_int0 - G_int0, rho, tau)) / 2
rH0 = Float64(biv_norm(H_int0 - H_int0, rho, tau)) / 2

biv_norm(S_tilde3, rho, tau)
biv_norm(T_tilde3, rho, tau)

# conditionning of the problem
normg = biv_norm(g_int, rho, tau)
normh = biv_norm(h_int, rho, tau)
norms = biv_norm(s_int, rho, tau)
normt = biv_norm(t_int, rho, tau)
normG = biv_norm(G_int, rho, tau)
normH = biv_norm(H_int, rho, tau)
normS = biv_norm(S_int, rho, tau)
normT = biv_norm(T_int, rho, tau)

# comparing the two a posteriori validation methods
Y1 = to_other_poly(y^(m+2*n-2), CBall, CBX, CBXY)
(Phi, _, rPhi, _) = val_fast_div_rem_interval(Y1, H_int, l, rho, tau)
E2 = biv_truncate(to_ball(S_tilde3, l) * to_ball(G_tilde3, l) + to_ball(T_tilde3, l) * to_ball(H_tilde3, l) - 1, l)
S_Phi = biv_mullow(to_ball(S_tilde3, l), Phi, l)
E2_Phi = biv_mullow(E2, Phi, l)
mu = max(biv_norm(G_tilde3, rho, tau), biv_norm(H_tilde3, rho, tau))
nu = max(biv_norm(S_tilde3, rho, tau), biv_norm(T_tilde3, rho, tau))

lambda1 = Float64(biv_norm(E2, rho, tau) +
    mu * biv_norm(shift_right(E2_Phi, m+n-1), rho, tau) +
    mu * biv_norm(E2, rho, tau) * rPhi)

lambda2 = 2*Float64(nu +
    mu * biv_norm(shift_right(S_Phi, n-1), rho, tau) +
    mu * biv_norm(S_tilde3, rho, tau) * rPhi)

lambda1_flat = Float64(biv_norm(E2, rho, tau) +
    mu * biv_norm(E2_Phi, rho, tau) +
    mu * biv_norm(E2, rho, tau) * rPhi)

lambda2_flat = 2*Float64(nu +
    mu * biv_norm(S_Phi, rho, tau) +
    mu * biv_norm(S_tilde3, rho, tau) * rPhi)
1

# conditioning
mu0 = max(biv_norm(biv_truncate(G_tilde3, 1), rho, tau), biv_norm(biv_truncate(H_tilde3, 1), rho, tau))
nu0 = max(biv_norm(biv_truncate(S_tilde3, 1), rho, tau), biv_norm(biv_truncate(T_tilde3, 1), rho, tau))

kappa0 = mu0 * nu0
kappa = mu * nu


# inverse for Euclidean division
Hrev = rev(H, H.length-1)
# k = 40
k = (m+2*n-2) - n
Hinv = fast_inv_fast(Hrev, l, k)
Hrev_tilde = to_other_poly(Hrev, CFloat, RX, RXY)
Hinv_tilde1 = fast_inv_fast(Hrev_tilde, l, k)
errHinv1 = Float64(biv_norm(to_other_poly(Hinv_tilde1, CBall, CBX, CBXY) - to_other_poly(Hinv, CBall, CBX, CBXY), rho, tau))
Hinv_tilde2 = fast_inv(Hrev_tilde, l, k)
errHinv2 = Float64(biv_norm(to_other_poly(Hinv_tilde2, CBall, CBX, CBXY) - to_other_poly(Hinv, CBall, CBX, CBXY), rho, tau))
Hinv_tilde3 = fast_inv(Hrev_tilde, l, k, refine=true)
errHinv3 = Float64(biv_norm(to_other_poly(Hinv_tilde3, CBall, CBX, CBXY) - to_other_poly(Hinv, CBall, CBX, CBXY), rho, tau))
Hrev_int = to_other_poly(Hrev, CBall, CBX, CBXY)
Hinv_int0 = fast_inv_fast(Hrev_int, l, k)
rinv0 = Float64(biv_norm(Hinv_int0 - Hinv_int0, rho, tau)) / 2
(Hinv_int1, rinv1) = val_fast_inv_interval_fast(Hrev_int, l, k, rho, tau)
rinv1
(Hinv_int2, rinv2) = val_fast_inv_interval(Hrev_int, l, k, rho, tau)
rinv2
(Hinv_int3, rinv3) = val_fast_inv_interval(Hrev_int, l, k, rho, tau, refine=true)
rinv3



# show(stdout, "text/latex", H)