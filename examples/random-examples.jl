
cd(@__DIR__)
using Pkg
Pkg.activate((@__DIR__) * "/..")
using AbstractAlgebra
using Nemo
using ValidatedHensel
using Printf


prec = 256
setprecision(BigFloat, prec)
set_precision!(Balls, prec)

l = 10

# Q[[X]][Y]/(x^l) for exact computation
import AbstractAlgebra: QQ
QX, x = power_series_ring(QQ, l, "x"; model=:capped_absolute)
QXY, y = polynomial_ring(QX,"y")

# R[[X]][Y]/(x^l) with 'prec' bit of precision for numerical computation
RX, _ = power_series_ring(CFloat, l, "x"; model=:capped_absolute)
RXY, _ = polynomial_ring(RX,"y")

# R[[X]][Y]/(x^l) with 'prec' bit of precision for rigorous numerical computation
CBX, _ = power_series_ring(CBall, l, "x"; model=:capped_absolute)
CBXY, _ = polynomial_ring(CBX, "y")


"""
    gen_random_QXY_poly(k::Int, l::Int, m; monic=false)

Generates a random polynomial P in QXY (bivariate polynomial in x,y with rational coefficients), where coefficients are taken "uniformly" in the interval [-m,m].

# Arguments
- `k`: the degree in y.
- `l`: the degree in x.
- `m`: the bound for the random coefficients.
- `monic`: if monic=true, then P is monic in y, i.e., the coefficient of y^k is 1 and the coefficients of x^j*y^k for j > 0 are 0.

# Returns
The random polynomial `P`.
"""
function gen_random_QXY_poly(k::Int, l::Int, m; monic=false)
    P = QXY(0)
    for i=0:k, j=0:l 
        c = Rational{BigInt}(simplest_rational_inside((2 * rand(CBall) - 1) * m))
        if monic && i==k
            c = Rational{BigInt}(j==0)
        end
        P += c * x^j * y^i
    end
    return P
end

function gen_random_QXY_poly_pos(k::Int, l::Int, m; monic=false)
    P = QXY(0)
    for i=0:k, j=0:l 
        c = Rational{BigInt}(simplest_rational_inside(rand(CBall) * m))
        if monic && i==k
            c = Rational{BigInt}(j==0)
        end
        P += c * x^j * y^i
    end
    return P
end

# gen_random_QXY_poly(3, 2, 10)
# gen_random_QXY_poly(3, 2, 10, monic=true)


# writes polynomials G,H (in QXY) into a file
function save_GH(filename::String, G, H)
    touch(filename)
    open(filename, "w") do file 
        println(file, G)
        println(file, H)
    end
end

# reads polynomials G,H (in QXY) from a file
function read_GH(filename::String)
    open(filename, "r") do file 
        G = eval(Meta.parse(readline(file)))
        H = eval(Meta.parse(readline(file)))
        return (G,H)
    end
end

# for i=1:10
#     filename = "test-" * string(i) * ".txt"
#     G = gen_random_QXY_poly(3, 2, 10)
#     H = gen_random_QXY_poly(3, 2, 10, monic=true)
#     save_GH(filename, G, H)
#     Gbis, Hbis = read_GH(filename)
#     print(Gbis-G); print(", "); print(Hbis-H); print("\n")
# end

function test_GH(G, H, l, rho, tau, prec, inputprec;
    verbose=false)
    
    F = biv_mullow(G, H, l)
    g = biv_truncate(G, 1)
    h = biv_truncate(H, 1)

    # set floating-point precision for the input
    setprecision(BigFloat, inputprec)
    set_precision!(Balls, inputprec)

    g_tilde = to_other_poly(g, CFloat, RX, RXY)
    h_tilde = to_other_poly(h, CFloat, RX, RXY)
    F_tilde = to_other_poly(F, CFloat, RX, RXY)
    F_int = to_other_poly(F, CBall, CBX, CBXY)

    # set floating-point precision for the validation
    setprecision(BigFloat, prec)
    set_precision!(Balls, prec)

    # numerical Bézout cofactors (modulo x) for the input
    _, s_tilde, t_tilde = gcdx(g_tilde,h_tilde)
    err_gcd = Float64(biv_norm(s_tilde * g_tilde + t_tilde * h_tilde - 1, rho, tau))
    if verbose 
        println("err_gcd = "*string(err_gcd))
    end

    # numerical Hensel lifting
    (G_tilde, H_tilde, S_tilde, T_tilde) =
    hensel_lifting(F_tilde, g_tilde, h_tilde, s_tilde, t_tilde, l, refine=true)

    # # symbolic Hensel lifting
    # _, s, t = gcdx(g, h)
    # (_,_,S,T) = intermediate_hensel_lifting(F, g, h, s, t, l)

    # norms to estimate the conditioning
    normG = Float64(biv_norm(G_tilde, rho, tau))
    normH = Float64(biv_norm(H_tilde, rho, tau))
    normS = Float64(biv_norm(S_tilde, rho, tau))
    normT = Float64(biv_norm(T_tilde, rho, tau))
    cond = max(normG, normH) * max(normS, normT)

    if verbose 
        println("conditioning: cond = "*string(cond))
    end

    # validated Hensel lifting
    r = Float64(Inf)
    relr = Float64(Inf)
    r2 = Float64(Inf)
    relr2 = Float64(Inf)
    try
        (_, _, r_temp) = val_hensel_lifting_alt_interval(F_int, to_ball(g_tilde, 1), to_ball(h_tilde, 1), to_ball(s_tilde, 1), to_ball(t_tilde, 1), l, rho, tau, refine=false)
        r = Float64(mag(r_temp))
        relr = r / max(normG, normH)
    catch
        if verbose 
            println("FAIL!")
        end
    end
    try
        (_, _, r_temp) = val_hensel_lifting_alt_interval(F_int, to_ball(g_tilde, 1), to_ball(h_tilde, 1), to_ball(s_tilde, 1), to_ball(t_tilde, 1), l, rho, tau, refine=true)
        r2 = Float64(mag(r_temp))
        relr2 = r2 / max(normG, normH)
    catch
        if verbose 
            println("FAIL!")
        end
    end

    if verbose
        println("r = " * string(r) * " ; relr = " * string(relr) * 
        " ; r2 = " * string(r2) * " ; relr2 = " * string(relr2))
    end

    return [err_gcd, normG, normH, normS, normT, cond, r, relr, r2, relr2]
end

function test_GH_cond(G, H, l, rho, tau, prec;
    refine=false, refine_factor=0.5, verbose=false)
    
    setprecision(BigFloat, prec)
    set_precision!(Balls, prec)

    F = biv_mullow(G, H, l)
    g = biv_truncate(G, 1)
    h = biv_truncate(H, 1)

    g_tilde = to_other_poly(g, CFloat, RX, RXY)
    h_tilde = to_other_poly(h, CFloat, RX, RXY)
    F_tilde = to_other_poly(F, CFloat, RX, RXY)

    # numerical Bézout cofactors (modulo x) for the input
    _, s_tilde, t_tilde = gcdx(g_tilde,h_tilde)
    err_gcd = Float64(biv_norm(s_tilde * g_tilde + t_tilde * h_tilde - 1, rho, tau))
    if verbose 
        println("err_gcd = "*string(err_gcd))
    end

    # numerical Hensel lifting
    (G_tilde, H_tilde, S_tilde, T_tilde) =
    hensel_lifting(F_tilde, g_tilde, h_tilde, s_tilde, t_tilde, l,
    refine=refine, refine_factor=refine_factor, rho=rho, tau=tau)

    # norms to estimate the conditioning
    normG = Float64(biv_norm(G_tilde, rho, tau))
    normH = Float64(biv_norm(H_tilde, rho, tau))
    normS = Float64(biv_norm(S_tilde, rho, tau))
    normT = Float64(biv_norm(T_tilde, rho, tau))
    cond = max(normG, normH) * max(normS, normT)

    if verbose 
        println("conditioning: cond = "*string(cond))
    end

    return cond
end


function experiment(n, k, l, m, rho, tau, prec, inputprec;
    verbose=false, read_examples=false)

    RES = zeros(Float64, n, 10)

    for i=1:n

        if verbose
            println("i = " * string(i))
        end

        # read example or generate new random one
        G = gen_random_QXY_poly(k, l, m)
        H = gen_random_QXY_poly(k, l, m, monic=true)
        filename = "random-examples/random-example-"*string(i)*".txt"
        if read_examples
            try
                (G,H) = read_GH(filename)
            catch
                println("WARNING: file "*filename*" does not exist; created new random example")
                save_GH(filename, G, H)
            end
        else
            save_GH(filename, G, H)
        end

        RES[i,:] = test_GH(G, H, l, rho, tau, prec, inputprec, verbose=verbose)
    end

    return RES
end

function gen_random_examples(n, k, l, m, rho, tau, prec;
    refine=false, refine_factor=0.5, verbose=false)

    setprecision(BigFloat, prec)
    set_precision!(Balls, prec)

    Gs = zeros(QXY, n)
    Hs = zeros(QXY, n)
    conds = zeros(Float64, n)

    for i=1:n 
        Gs[i] = gen_random_QXY_poly(k, l, m)
        Hs[i] = gen_random_QXY_poly(k, l, m, monic=true)
        try
            conds[i] = test_GH_cond(Gs[i], Hs[i], l, rho, tau, prec, refine=refine, refine_factor=refine_factor, verbose=verbose)
        catch
            conds[i] = Float64(Inf)
        end
    end

    # indices for the sorting after condition number
    ind = sortperm(conds)

    conds = conds[ind]
    Gs = Gs[ind]
    Hs = Hs[ind]

    for i=1:n
        save_GH("random-examples/random-example-"*string(i)*".txt", Gs[i], Hs[i])
    end

    return Gs, Hs, conds
end



n = 30
k = 10
l = 10
m = 10
rho = 1.0
tau = 1.0
prec = 256
inputprec = 256

## generate 30 random examples sorted by conditioning ##
# Gs, Hs, conds = gen_random_examples(n, k, l, m, rho, tau, prec, refine=true)
# show(stdout, "text/plain", conds)

## experiment on the random examples ##
RES = experiment(n, k, l, m, rho, tau, prec, inputprec, verbose=true, read_examples=true)

show(stdout, "text/plain", RES)


# print Latex table using Printf

for i=1:15
    j = i+15
    @printf "%d\t&\t%.2e\t&\t%.2e\t&\t%.2e\t&\t&\t%d\t&\t%.2e\t&\t%.2e\t&\t%.2e\t\\\\\n" i RES[i,6] RES[i,8] RES[i,10] j RES[j,6] RES[j,8] RES[j,10]
end


## try to execute Hensel's lifting in rational arithmetic on an example ##

(G,H) = read_GH("random-examples-ref/random-example-1.txt")
g = biv_truncate(G, 1)
h = biv_truncate(H, 1)
gcd_gh, s, t = gcdx(g, h)
F = biv_mullow(G, H, l)
@time begin (Gbis, Hbis, S, T) = intermediate_hensel_lifting(F, g, h, s, t, l); true end