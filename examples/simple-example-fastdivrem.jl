################################################################################
#
#   Objects definition
#
################################################################################

cd(@__DIR__)
using Pkg
Pkg.activate((@__DIR__) * "/..")
using Printf
using AbstractAlgebra
using Nemo
using ValidatedHensel

setprecision(BigFloat, 64)
set_precision!(Balls, 64)

k = 200
l = 201

# Q[[X]][Y]/(x^l) for exact computation
QX, x = power_series_ring(AbstractAlgebra.QQ, l, "x"; model=:capped_absolute)
QXY, y = polynomial_ring(QX,"y")

################################################################################
#
#   Example
#
################################################################################

function test_example_fast_inv(G, k, l, rho, tau, prec, inputprec)

    # exact numerical inverse
    Ginv = fast_inv_fast(G, l, k)

    # R[[X]][Y]/(x^l) with 64-bit of precision for numerical computation
    RX, _ = power_series_ring(CFloat, l, "x"; model=:capped_absolute)
    RXY, _ = polynomial_ring(RX,"y")

    # R[[X]][Y]/(x^l) with 64-bit of precision for rigorous numerical computation
    CBX, _ = power_series_ring(CBall, l, "x"; model=:capped_absolute)
    CBXY, _ = polynomial_ring(CBX, "y")

    norm = biv_norm(to_other_poly(Ginv, CBall, CBX, CBXY), rho, tau)

    # set floating-point precision for the input
    setprecision(BigFloat, inputprec)
    set_precision!(Balls, inputprec)

    G_int = to_other_poly(G, CBall, CBX, CBXY)
    # print(G_int)

    # set floating-point precision for the validation
    setprecision(BigFloat, prec)
    set_precision!(Balls, prec)

    # various validated routines to compute the inverse

    # straightforward interval computation
    Ginv_int1 = fast_inv_fast(G_int, l, k)
    r1 = biv_norm(Ginv_int1 - Ginv_int1, rho, tau)/2
    
    # a posteriori validation, numerical approximation with no overwriting, no refinement
    (Ginv_int2,r2) = val_fast_inv_interval_fast(G_int, l, k, rho, tau)
    r2_exact = biv_norm(Ginv_int2 - to_other_poly(Ginv, CBall, CBX, CBXY), rho, tau)

    # a posteriori validation, numerical approximation with overwriting but no refinement
    (Ginv_int3,r3) = val_fast_inv_interval(G_int, l, k, rho, tau)
    r3_exact = biv_norm(Ginv_int3 - to_other_poly(Ginv, CBall, CBX, CBXY), rho, tau)

    # a posteriori validation, numerical approximation with overwriting and refinement
    (Ginv_int4,r4) = val_fast_inv_interval(G_int, l, k, rho, tau, refine=true)
    r4_exact = biv_norm(Ginv_int4 - to_other_poly(Ginv, CBall, CBX, CBXY), rho, tau)

    return Float64.([norm, r1, r2, r2_exact, r3, r3_exact, r4, r4_exact])
end


function test_example_fast_div_rem(F, G, l, rho, tau, prec, inputprec)

    # exact Euclidean division
    (Q,R) = fast_div_rem_fast(F, G, l)

    # R[[X]][Y]/(x^l) with 64-bit of precision for numerical computation
    RX, _ = power_series_ring(CFloat, l, "x"; model=:capped_absolute)
    RXY, _ = polynomial_ring(RX,"y")

    # R[[X]][Y]/(x^l) with 64-bit of precision for rigorous numerical computation
    CBX, _ = power_series_ring(CBall, l, "x"; model=:capped_absolute)
    CBXY, _ = polynomial_ring(CBX, "y")

    normQ = biv_norm(to_other_poly(Q, CBall, CBX, CBXY), rho, tau)
    normR = biv_norm(to_other_poly(R, CBall, CBX, CBXY), rho, tau)

    # set floating-point precision for the input
    setprecision(BigFloat, inputprec)
    set_precision!(Balls, inputprec)

    F_int = to_other_poly(F, CBall, CBX, CBXY)
    G_int = to_other_poly(G, CBall, CBX, CBXY)

    # set floating-point precision for the validation
    setprecision(BigFloat, prec)
    set_precision!(Balls, prec)

    (Q_int, R_int, rQ, rR) = val_fast_div_rem_interval(F_int, G_int, l, rho, tau, refine=true)

    rQ_exact = biv_norm(Q_int - to_other_poly(Q, CBall, CBX, CBXY), rho, tau)
    rR_exact = biv_norm(R_int - to_other_poly(R, CBall, CBX, CBXY), rho, tau)

    (Q_int2, R_int2) = fast_div_rem_fast(F_int, G_int, l)
    rQ2 = biv_norm(Q_int2 - Q_int2, rho, tau)/2
    rR2 = biv_norm(R_int2 - R_int2, rho, tau)/2

    return Float64.([normQ, rQ_exact, rQ, rQ2, normR, rR_exact, rR, rR2])
end




# simple example 

# Grev = 1 + (1//3+2//7*x)*y + (1-3//5*x^2)*y^2
# Grev = 1 + (1//3+2//7*x)*y + (3//5-3//15*x^2)*y^2
Grev = 1 + (4//9+3//7*x)*y + (1//5-3//10*x^2)*y^2
rho = 1.0
tau = 1.0
prec = 64
inputprec = 50

RES = zeros(Float64, 20, 10)

for i = 1:20
    println("i=",i)
    kk = 10*i-1
    ll = 10*i+1
    RES[i,:] = vcat(Float64.([kk,ll]), test_example_fast_inv(Grev, kk, ll, rho, tau, prec, inputprec))
end

RES
RES[1:10,:]





Ginv = fast_inv(Grev, 200, 200)
CBX, _ = power_series_ring(CBall, l, "x"; model=:capped_absolute)
CBXY, _ = polynomial_ring(CBX, "y")
biv_norm(to_other_poly(Ginv, CBall, CBX, CBXY), rho, tau)

G = rev(Grev, 2)
# F1 = y - x + 1//2*x^2
# F10 = F1^10
# F = F1^2

prec = 64
inputprec = 50

RES2 = zeros(Float64, 20, 10)

@time for i = 1:20
    println("i=",i)
    kk = 10*i
    ll = 10*i+1 
    F = y^kk
    for j=1:kk
        F += x^j * y^(kk-j)
    end
    RES2[i,:] = vcat(Float64.([kk,ll]), test_example_fast_div_rem(F, G, ll, rho, tau, prec, inputprec))
end

RES2[1:10,:]



## PRINT RESULTS ##

for i = 1:20
@printf "%d & %.1f & %.1f & %.1f & %1.1e & %1.1e & %1.1e & %1.1e & %1.1e & %1.1e & %1.1e & %1.1e & %1.1e \\\\\n" RES2[i,1] RES[i,3] RES2[i,3] RES2[i,7] RES[i,10] RES2[i,4] RES2[i,8] RES[i,9] RES2[i,5] RES2[i,9] RES[i,4] RES2[i,6] RES2[i,10]
end



## STOP HERE ##


CBX, _ = power_series_ring(CBall, 100, "x"; model=:capped_absolute)
CBXY, _ = polynomial_ring(CBX, "y")
Ginv = fast_inv(G, 100, 100)
Float64(biv_norm(to_other_poly(Ginv, CBall, CBX, CBXY), rho, tau))