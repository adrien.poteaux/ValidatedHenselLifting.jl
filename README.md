# ValidatedHenselLifting.jl

Numerically validated Hensel's lifting over R\[\[x\]\]\[y\] or C\[\[x\]\]\[y\], with guaranteed enclosures.

<!-- This package implements the algorithms presented in-->

## Installation

```julia
julia> ]
(v1.7) pkg> add https://gitlab.univ-lille.fr/adrien.poteaux/ValidatedHenselLifting.jl
```

This package depends on:

* [AbstractAlgebra.jl](https://nemocas.github.io/AbstractAlgebra.jl/) –
computer algebra package for the Julia programming language, implementing basic data structures such as polynomials.

* [Nemo.jl](https://github.com/JuliaMath/Polynomials.jl) –
computer algebra package for the Julia programming language, implementing concrete data types such as rational numbers and interval arithmetic

## Authors <!-- and acknowledgment -->

* [Florent Bréhard](https://pro.univ-lille.fr/florent-brehard) – Université de Lille, CNRS, Centrale Lille, UMR 9189 CRIStAL, F-59000 Lille, France

* [Adrien Poteaux](https://www.fil.univ-lille.fr/~poteaux/) – Université de Lille, CNRS, Centrale Lille, UMR 9189 CRIStAL, F-59000 Lille, France

* Arthur Vinciguerra – École normale supérieure de Lyon, Lyon, France

* Jasmin Krueger  – Leibniz Universität, Hannover, Germany
